---
wsId: geminiwallet
title: "Gemini: Buy Bitcoin Instantly"
altTitle: 
authors:
- leo
appId: com.gemini.ios
appCountry: 
idd: 1408914447
released: 2018-12-11
updated: 2021-04-07
version: "3.10.0"
score: 4.7158
reviews: 22037
size: 104160256
developerWebsite: http://gemini.com
repository: 
issue: 
icon: com.gemini.ios.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: gemini
providerLinkedIn: geminitrust
providerFacebook: GeminiTrust
providerReddit: 

redirect_from:

---

This provider being an exchange, together with the lack of clear words of who
gets to hold the private keys leads us to believe this app is only an interface
to the Gemini exchange account and thus custodial and thus **not verifiable**.
