---
wsId: mwallet
title: "Bitcoin Wallet: buy BTC & BCH"
altTitle: 
authors:
- leo
appId: com.bitcoin.mwallet
appCountry: 
idd: 1252903728
released: 2017-07-11
updated: 2021-04-07
version: "6.12.0"
score: 4.29038
reviews: 5176
size: 125095936
developerWebsite: https://www.bitcoin.com
repository: 
issue: 
icon: com.bitcoin.mwallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

