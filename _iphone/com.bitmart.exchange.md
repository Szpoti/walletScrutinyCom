---
wsId: bitmart
title: "BitMart - Crypto Exchange"
altTitle: 
authors:
- leo
appId: com.bitmart.exchange
appCountry: 
idd: 1396382871
released: 2018-08-02
updated: 2021-04-07
version: "2.4.8"
score: 3.17808
reviews: 73
size: 98089984
developerWebsite: https://www.bitmart.com/
repository: 
issue: 
icon: com.bitmart.exchange.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

