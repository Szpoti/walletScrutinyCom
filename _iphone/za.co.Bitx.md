---
wsId: Luno
title: "Luno Bitcoin & Cryptocurrency"
altTitle: 
authors:
- leo
appId: za.co.Bitx
appCountry: 
idd: 927362479
released: 2014-11-03
updated: 2021-04-07
version: "7.11.0"
score: 4.43714
reviews: 3166
size: 93165568
developerWebsite: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

