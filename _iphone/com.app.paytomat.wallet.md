---
wsId: paytomat
title: "Paytomat Wallet"
altTitle: 
authors:
- leo
appId: com.app.paytomat.wallet
appCountry: 
idd: 1415300709
released: 2018-08-12
updated: 2021-02-03
version: "1.37.2"
score: 4.38095
reviews: 21
size: 65810432
developerWebsite: https://paytomat.com
repository: 
issue: 
icon: com.app.paytomat.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

