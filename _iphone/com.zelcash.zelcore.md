---
wsId: ZelCore
title: "ZelCore"
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2021-04-02
version: "v4.8.0"
score: 4.4902
reviews: 51
size: 64811008
developerWebsite: https://zel.network/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

