---
wsId: 
title: "Crypto Exchange Currency.com"
altTitle: 
authors:
- leo
users: 100000
appId: com.currency.exchange.prod2
launchDate: 
latestUpdate: 2021-04-05
apkVersionName: "1.11.1"
stars: 4.1
ratings: 2124
reviews: 783
size: Varies with device
website: https://currency.com
repository: 
issue: 
icon: com.currency.exchange.prod2.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: currencycom
providerLinkedIn: currencycom/
providerFacebook: currencycom/
providerReddit: 

redirect_from:
  - /com.currency.exchange.prod2/
  - /posts/com.currency.exchange.prod2/
---


This is an interface for a custodial trading platform and thus **not
verifiable**.
