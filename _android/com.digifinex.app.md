---
wsId: 
title: "DigiFinex - Buy & Sell Bitcoin, Crypto Trading"
altTitle: 
authors:
- leo
users: 100000
appId: com.digifinex.app
launchDate: 
latestUpdate: 2021-04-01
apkVersionName: "2021.04.01"
stars: 3.6
ratings: 2319
reviews: 1448
size: 66M
website: https://www.digifinex.com
repository: 
issue: 
icon: com.digifinex.app.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-28
reviewStale: true
signer: 
reviewArchive:


providerTwitter: DigiFinex
providerLinkedIn: digifinex-global
providerFacebook: digifinex.global
providerReddit: DigiFinex

redirect_from:
  - /com.digifinex.app/
---


> DigiFinex is a world’s leading crypto finance exchange

doesn't sound like "wallet" is their primary business and as we can't find any
claims to the contrary, we have to assume this is a custodial offering and thus
**not verifiable**.
