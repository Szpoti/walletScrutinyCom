---
wsId: 
title: "Ledger Live"
altTitle: 
authors:

users: 100000
appId: com.ledger.live
launchDate: 
latestUpdate: 2021-04-07
apkVersionName: "2.24.0"
stars: 3.6
ratings: 3379
reviews: 1947
size: Varies with device
website: 
repository: 
issue: 
icon: com.ledger.live.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-17
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Ledger
providerLinkedIn: ledgerhq
providerFacebook: Ledger
providerReddit: 

redirect_from:
  - /com.ledger.live/
---


This is the companion app for the Ledger hardware wallets. As we fail to start
the app without connecting a hardware wallet, it is very likely this app is not
designed to store private keys or in other words it is probably impossible to
spend with this app without confirming each and every transaction on the
hardware wallet itself and is thus **not a wallet** itself.
