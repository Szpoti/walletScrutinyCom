---
wsId: 
title: "eXchangily DEX Bitcoin Wallet & Crypto Wallet"
altTitle: 
authors:

users: 500
appId: com.exchangily.wallet
launchDate: 
latestUpdate: 2021-04-07
apkVersionName: "2.0.15"
stars: 4.0
ratings: 25
reviews: 18
size: 16M
website: 
repository: 
issue: 
icon: com.exchangily.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/187 -->
