---
wsId: 
title: "Multi Crypto Wallet: for Bitcoin and 20 currencies"
altTitle: 
authors:
- leo
users: 1000
appId: appinventor.ai_oldgoldmen.CryptoWallet
launchDate: 
latestUpdate: 2021-02-23
apkVersionName: "3.0"
stars: 4.6
ratings: 38
reviews: 17
size: 3.3M
website: 
repository: 
issue: 
icon: appinventor.ai_oldgoldmen.CryptoWallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /appinventor.ai_oldgoldmen.CryptoWallet/
---


