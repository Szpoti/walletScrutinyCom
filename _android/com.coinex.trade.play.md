---
wsId: CoinEx
title: "CoinEx"
altTitle: 
authors:
- leo
users: 100000
appId: com.coinex.trade.play
launchDate: 
latestUpdate: 2021-04-03
apkVersionName: "1.9.5.2"
stars: 4.7
ratings: 7466
reviews: 2498
size: 14M
website: https://www.coinex.co
repository: 
issue: 
icon: com.coinex.trade.play.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: coinexcom
providerLinkedIn: 
providerFacebook: TheCoinEx
providerReddit: Coinex

redirect_from:
  - /com.coinex.trade.play/
  - /posts/com.coinex.trade.play/
---


The description on Google Play starts not so promising:

> Meet the top cryptocurrency trading app！

Trading apps are usually custodial. Unfortunately there is no easily accessible
information on their website about the app neither. For now we assume it is
custodial and thus **not verifiable**.
