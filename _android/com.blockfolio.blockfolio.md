---
wsId: 
title: "Blockfolio - Bitcoin and Cryptocurrency Tracker"
altTitle: 
authors:

users: 1000000
appId: com.blockfolio.blockfolio
launchDate: 2015-10-01
latestUpdate: 2021-04-03
apkVersionName: "3.0.26"
stars: 4.5
ratings: 134780
reviews: 40903
size: 53M
website: https://www.blockfolio.com
repository: 
issue: 
icon: com.blockfolio.blockfolio.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /blockfolio/
  - /com.blockfolio.blockfolio/
  - /posts/2019/11/blockfolio/
  - /posts/com.blockfolio.blockfolio/
---


This is not a wallet.
