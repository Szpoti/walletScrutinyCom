---
wsId: bitso
title: "Bitso - Buy and sell bitcoin"
altTitle: 
authors:
- leo
users: 1000000
appId: com.bitso.wallet
launchDate: 
latestUpdate: 2021-03-30
apkVersionName: "2.24.1"
stars: 3.9
ratings: 8363
reviews: 4901
size: 27M
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: Bitso
providerLinkedIn: 
providerFacebook: bitsoex
providerReddit: 

redirect_from:
  - /com.bitso.wallet/
---


Bitso appears to be an exchange and as so often, we see no mentions of security
in the app description or the website and have to assume it is a custodial
offering and thus **not verifiable**.
